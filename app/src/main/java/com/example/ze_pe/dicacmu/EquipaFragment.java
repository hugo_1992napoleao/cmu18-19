package com.example.ze_pe.dicacmu;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.example.ze_pe.dicacmu.API.Equipa;
import com.example.ze_pe.dicacmu.API.EquipaAdapter;
import com.example.ze_pe.dicacmu.API.Jogos;
import com.example.ze_pe.dicacmu.API.JogosAdapter;
import com.example.ze_pe.dicacmu.API.Liga;
import com.example.ze_pe.dicacmu.Dialog.pesquisaAnoDialog;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class EquipaFragment extends Fragment {

    private Context mContext;
    private View mContentView;
    private RecyclerView mEquipaView;
    private EquipaAdapter mEquipaAdapter;
    private AutoCompleteTextView autoCompleteTextView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.fragment_equipa, container, false);
        List<Equipa> equipaList = getArguments().getParcelableArrayList("Equipa");
        List<Liga> ligas = getArguments().getParcelableArrayList("Liga");

        autoCompleteTextView = mContentView.findViewById(R.id.LigaEquipa);
        LigaAdpter ligaAdpter = new LigaAdpter(mContext, ligas);
        autoCompleteTextView.setAdapter(ligaAdpter);

        mEquipaAdapter = new EquipaAdapter(mContext, equipaList);
        mEquipaView = mContentView.findViewById(R.id.resultadoEquipa);
        mEquipaView.setAdapter(mEquipaAdapter);
        mEquipaView.setLayoutManager(new LinearLayoutManager(mContext));


        //Criação do toolbar

        Toolbar pesquisa = mContentView.findViewById(R.id.pesquisa_equipa);
        ((AppCompatActivity)getActivity()).setSupportActionBar(pesquisa);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Equipa");


        return mContentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        (getActivity()).getMenuInflater().inflate(R.menu.menu_equipa, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*switch (item.getItemId()) {
            case R.id.procurar_jogos_estado:
                openDialogEstado();
                break;
            case R.id.procurar_jogos_dias:
                openDialogData();
                break;
        }*/
        return true;
    }

    public void openDialogData() {
        pesquisaAnoDialog ano = new pesquisaAnoDialog();
        ano.show(getFragmentManager(), "data");
    }

    public void openDialogEstado() {
        Log.d("dialog", "openDialogEstado: ");
    }

}
