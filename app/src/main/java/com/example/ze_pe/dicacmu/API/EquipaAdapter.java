package com.example.ze_pe.dicacmu.API;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ze_pe.dicacmu.Detalhes;
import com.example.ze_pe.dicacmu.R;

import java.util.List;

public class EquipaAdapter extends RecyclerView.Adapter<EquipaAdapter.EquipaViewHolder> {

    private Context mContext;
    private List<Equipa> mEquipa;

    public EquipaAdapter(Context mContext, List<Equipa> mEquipa) {
        this.mContext = mContext;
        this.mEquipa = mEquipa;

    }

    @NonNull
    @Override
    public EquipaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View JogosView = inflater.inflate(R.layout.item_equipa, viewGroup, false);

        return new EquipaViewHolder(JogosView);
    }

    @Override
    public void onBindViewHolder(@NonNull EquipaViewHolder ViewHolder, int i) {
        final Equipa equipa = mEquipa.get(i);

        ImageView logo = ViewHolder.equipaLogo;
        TextView nome = ViewHolder.equipaName;
        TextView shortName = ViewHolder.shortName;

        //logo.setText(equipa.getCrestUrl());
        nome.setText(equipa.getName());
        shortName.setText(equipa.getShortName());

        nome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, Detalhes.class);
                intent.putExtra("detalhes", equipa);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mEquipa.size();
    }

    public class EquipaViewHolder extends RecyclerView.ViewHolder {

        public ImageView equipaLogo;
        public TextView equipaName;
        public TextView shortName;

        public EquipaViewHolder(@NonNull View itemView) {
            super(itemView);
            equipaLogo = itemView.findViewById(R.id.equipa_logo);
            equipaName = itemView.findViewById(R.id.equipa_name);
            shortName = itemView.findViewById(R.id.equipa_short_name);
        }
    }
}
