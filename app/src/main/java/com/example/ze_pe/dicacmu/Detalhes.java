package com.example.ze_pe.dicacmu;

import android.arch.persistence.room.Room;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.ze_pe.dicacmu.API.Equipa;
import com.example.ze_pe.dicacmu.API.Jogos;
import com.example.ze_pe.dicacmu.Logic.Favoritos;
import com.example.ze_pe.dicacmu.Logic.FavoritosDataBase;

public class Detalhes extends AppCompatActivity {
    private Parcelable data;
    private TextView Info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        data = getIntent().getParcelableExtra("detalhes");
        Info = findViewById(R.id.InfoTotal);
        Info.setText(carregarDados());

        //Toolbar
        Toolbar pesquisa = findViewById(R.id.barra);
        setSupportActionBar(pesquisa);
        getSupportActionBar().setTitle("Detalhes");


    }

    private String carregarDados() {
        String aux = "";
        if (data instanceof Equipa) {
            aux += "Nome " + ((Equipa) data).getName() + "\n"
                    + "Abreviação " + ((Equipa) data).getShortName() + "\n"
                    + "Ano de Fundação " +  ((Equipa) data).getFounded() + "\n"
                    + "Morada " + ((Equipa) data).getAddress() + "\n";
        }

        if (data instanceof Jogos) {
            aux += "Equipa da casa " + ((Jogos) data).getHomeTeam().getName() + "\n"
                    + "Equipa visitante " + ((Jogos) data).getAwayTeam().getName() + "\n"
                    + "Estado da partida " +  ((Jogos) data).getStatus() + "\n"
                    + "Data da partida " + ((Jogos) data).getUtcDate() + "\n";
        }
        return aux;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detalhes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favorito_adicionado:
                adicionarFavoritos();
            break;
        }
        return true;
    }

    private void adicionarFavoritos() {
        Favoritos favoritos = null;
        if (data instanceof Equipa) {
            favoritos = new Favoritos(((Equipa) data).getId(), ((Equipa) data).getName(),"Equipa");
        } else if(data instanceof Jogos) {
            favoritos = new Favoritos(((Jogos) data).getId(),((Jogos) data).getHomeTeam().getName() + "\n vs \n" + ((Jogos) data).getAwayTeam().getName(), "Jogos");
        }

        if (favoritos != null) {
            final FavoritosDataBase dataBase = Room.databaseBuilder(Detalhes.this, FavoritosDataBase.class, "prodution")
                    .allowMainThreadQueries()
                    .build();
            dataBase.favoritosDAO().insertFavoritos(favoritos);
        }
    }
}
