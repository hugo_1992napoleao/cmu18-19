package com.example.ze_pe.dicacmu.API;

import android.os.Parcel;
import android.os.Parcelable;

public class Jogador implements Parcelable {
    private int id;
    private String name;
    private String firstName;
    private String nationality;
    private String position;

    protected Jogador(Parcel in) {
        id = in.readInt();
        name = in.readString();
        firstName = in.readString();
        nationality = in.readString();
        position = in.readString();
    }

    public static final Creator<Jogador> CREATOR = new Creator<Jogador>() {
        @Override
        public Jogador createFromParcel(Parcel in) {
            return new Jogador(in);
        }

        @Override
        public Jogador[] newArray(int size) {
            return new Jogador[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getNationality() {
        return nationality;
    }

    public String getPosition() {
        return position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(firstName);
        dest.writeString(nationality);
        dest.writeString(position);
    }
}
