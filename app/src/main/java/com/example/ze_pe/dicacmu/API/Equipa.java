package com.example.ze_pe.dicacmu.API;

import android.os.Parcel;
import android.os.Parcelable;

public class Equipa implements Parcelable {
    private int id;
    private Area area;
    private String name;
    private String shortName;
    private String crestUrl;
    private String address;
    private int founded;

    protected Equipa(Parcel in) {
        id = in.readInt();
        area = in.readParcelable(Area.class.getClassLoader());
        name = in.readString();
        shortName = in.readString();
        crestUrl = in.readString();
        address = in.readString();
        founded = in.readInt();
    }

    public static final Creator<Equipa> CREATOR = new Creator<Equipa>() {
        @Override
        public Equipa createFromParcel(Parcel in) {
            return new Equipa(in);
        }

        @Override
        public Equipa[] newArray(int size) {
            return new Equipa[size];
        }
    };

    public int getId() {
        return id;
    }

    public Area getArea() {
        return area;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    public String getCrestUrl() {
        return crestUrl;
    }

    public String getAddress() {
        return address;
    }

    public int getFounded() {
        return founded;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeParcelable(area, flags);
        dest.writeString(name);
        dest.writeString(shortName);
        dest.writeString(crestUrl);
        dest.writeString(address);
        dest.writeInt(founded);
    }
}
