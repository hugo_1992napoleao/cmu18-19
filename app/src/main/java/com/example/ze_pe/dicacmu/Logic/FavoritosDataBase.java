package com.example.ze_pe.dicacmu.Logic;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Favoritos.class}, version = 1)
public abstract class FavoritosDataBase extends RoomDatabase {
    public abstract FavoritosDAO favoritosDAO();

}
