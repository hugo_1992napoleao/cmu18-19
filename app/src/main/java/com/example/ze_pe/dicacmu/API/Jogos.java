package com.example.ze_pe.dicacmu.API;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Jogos implements Parcelable {
    private int id;
    private Date utcDate;
    private String status;
    private Team homeTeam;
    private Team awayTeam;

    public Jogos(int id, Date utcDate, String status, Team homeTeam, Team awayTeam) {
        this.id = id;
       // this.utcDate = utcDate;
        this.status = status;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }


    protected Jogos(Parcel in) {
        id = in.readInt();
        status = in.readString();
        homeTeam = in.readParcelable(Team.class.getClassLoader());
        awayTeam = in.readParcelable(Team.class.getClassLoader());
    }

    public static final Creator<Jogos> CREATOR = new Creator<Jogos>() {
        @Override
        public Jogos createFromParcel(Parcel in) {
            return new Jogos(in);
        }

        @Override
        public Jogos[] newArray(int size) {
            return new Jogos[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getUtcDate() {
        return utcDate;
    }

    public void setUtcDate(Date utcDate) {
        this.utcDate = utcDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    @Override
    public String toString() {
        return "Jogos{" +
                "id=" + id +
                ", utcDate=" + utcDate +
                ", status='" + status + '\'' +
                ", homeTeam=" + homeTeam +
                ", awayTeam=" + awayTeam +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(status);
        dest.writeParcelable(homeTeam, flags);
        dest.writeParcelable(awayTeam, flags);
    }
}
