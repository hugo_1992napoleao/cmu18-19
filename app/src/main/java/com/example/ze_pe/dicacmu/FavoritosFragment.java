package com.example.ze_pe.dicacmu;


import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.ze_pe.dicacmu.API.Jogos;
import com.example.ze_pe.dicacmu.API.JogosAdapter;
import com.example.ze_pe.dicacmu.Dialog.pesquisaAnoDialog;
import com.example.ze_pe.dicacmu.Logic.Favoritos;
import com.example.ze_pe.dicacmu.Logic.FavoritosDataBase;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritosFragment extends Fragment {

    private Context mContext;
    private View mContentView;
    private RecyclerView mFavortosView;
    private FavoriteAdapter mFavoriteAdapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.fragment_favoritos, container, false);
        List<Favoritos> favoritosList  = createList();

        mFavoriteAdapter = new FavoriteAdapter(mContext, favoritosList);
        mFavortosView = mContentView.findViewById(R.id.resultadoFavo);
        mFavortosView.setAdapter(mFavoriteAdapter);
        mFavortosView.setLayoutManager(new LinearLayoutManager(mContext));

        //Criação do toolbar

        Toolbar pesquisa = mContentView.findViewById(R.id.pesquisa_favo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(pesquisa);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Jogos");

        return mContentView;
    }

    private List<Favoritos> createList() {
        final FavoritosDataBase dataBase = Room.databaseBuilder(mContext, FavoritosDataBase.class, "prodution")
                .allowMainThreadQueries()
                .build();
        return dataBase.favoritosDAO().getAllFavoritos();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        (getActivity()).getMenuInflater().inflate(R.menu.menu_jogos, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.procurar_jogos_estado:
                openDialogEstado();
                break;
            case R.id.procurar_jogos_dias:
                openDialogData();
                break;
        }
        return true;
    }

    public void openDialogData() {
        pesquisaAnoDialog ano = new pesquisaAnoDialog();
        ano.show(getFragmentManager(), "data");
    }

    public void openDialogEstado() {
        Log.d("dialog", "openDialogEstado: ");
    }

}
