package com.example.ze_pe.dicacmu;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.ze_pe.dicacmu.API.Eq;
import com.example.ze_pe.dicacmu.API.FootballDataAPI;
import com.example.ze_pe.dicacmu.API.JogadorCall;
import com.example.ze_pe.dicacmu.API.LOJ;
import com.example.ze_pe.dicacmu.API.Liga;
import com.example.ze_pe.dicacmu.API.LigaCall;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Menu extends AppCompatActivity {
    private static String plan = "TIER_ONE";
    private List<Liga> ligas;
    private int idLiga = 2003;
    private int idJogador = 44;

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                    switch (menuItem.getItemId()) {
                        case R.id.jogo_btn:
                            getAllJogos(idLiga);
                            break;
                        case R.id.equipa_btn:
                            getAllEquipa(idLiga);
                            break;
                        case R.id.jogador_btn:
                            Toast.makeText(Menu.this, "chamando jogador", Toast.LENGTH_SHORT).show();
                            getJogadorbyId(idJogador);
                            break;
                        case R.id.favorito_btn:
                            loadDataFavoritos();
                            break;
                    }
                    return true;
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getLigas();

        idLiga = getIntent().getIntExtra("liga", 2003);

        //Criação do toolbar
        BottomNavigationView bottNav = findViewById(R.id.pesquisa_bar);
        bottNav.setOnNavigationItemSelectedListener(navListener);
    }

    private Retrofit getRetrofit() {
        return new Retrofit.Builder().baseUrl("http://api.football-data.org/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private FootballDataAPI getAPI() {
        return getRetrofit().create(FootballDataAPI.class);
    }

    private void getAllJogos(int id) {
        getAPI().getAllGamesOfcompetition(id).enqueue(new Callback<LOJ>() {
            @Override
            public void onResponse(Call<LOJ> call, Response<LOJ> response) {
                Log.d("error_games", "onResponse");
                loadDataJogos(response.body());
            }

            @Override
            public void onFailure(Call<LOJ> call, Throwable t) {
                Log.d("error_games", "onFailure: " + t.toString());
            }
        });
    }

    private void loadDataJogos(LOJ response) {
        //Criar o fragment
        JogosFragment jogosFragment = new JogosFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("jogos", (ArrayList<? extends Parcelable>) response.matches);
        jogosFragment.setArguments(args);

        //Carregar fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.pesquisa_frag, jogosFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void getJogosEstados(int id) {
        getAPI().getAllGamesformStatus(id, "SCHEDULED").enqueue(new Callback<LOJ>() {
            @Override
            public void onResponse(Call<LOJ> call, Response<LOJ> response) {
                Log.d("error_games", "onResponse");
                loadDataJogos(response.body());
            }

            @Override
            public void onFailure(Call<LOJ> call, Throwable t) {
                Log.d("error_games", "onFailure: " + t.toString());
            }
        });
    }

    public void getJogosAno(int id, Date inicio, Date fim) {
        getAPI().getAllGamesformdate(id, inicio, fim).enqueue(new Callback<LOJ>() {
            @Override
            public void onResponse(Call<LOJ> call, Response<LOJ> response) {
                Log.d("error_games", "onResponse");
                loadDataJogos(response.body());
            }

            @Override
            public void onFailure(Call<LOJ> call, Throwable t) {
                Log.d("error_games", "onFailure: " + t.toString());
            }
        });
    }


    private void loadDataEquipa(Eq response) {
        //Criar o fragment
        EquipaFragment equipaFragment = new EquipaFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("Equipa", (ArrayList<? extends Parcelable>) response.teams);
        args.putParcelableArrayList("Liga", (ArrayList<? extends Parcelable>) ligas);
        equipaFragment.setArguments(args);

        //Carregar fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.pesquisa_frag, equipaFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void getAllEquipa(int id) {
        getAPI().getAllEquipasOfcompetition(id).enqueue(new Callback<Eq>() {
            @Override
            public void onResponse(Call<Eq> call, Response<Eq> response) {
                Log.d("error_equipa", "onResponse");
                loadDataEquipa(response.body());
            }

            @Override
            public void onFailure(Call<Eq> call, Throwable t) {
                Log.d("error_equipa", "onFailure: " + t.toString());
            }
        });
    }

    private void loadDataJogador(JogadorCall response) {
        //Criar o fragment
        JogadorFragment jogadorFragment = new JogadorFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("Jogador", (ArrayList<? extends Parcelable>) response.player);
        jogadorFragment.setArguments(args);

        //Carregar fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.pesquisa_frag, jogadorFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void getJogadorbyId(int id) {
        getAPI().getJogadorOfcompetition(id, plan).enqueue(new Callback<JogadorCall>() {
            @Override
            public void onResponse(Call<JogadorCall> call, Response<JogadorCall> response) {
                Log.d("error_jogador", "onResponse");
                loadDataJogador(response.body());
            }

            @Override
            public void onFailure(Call<JogadorCall> call, Throwable t) {
                Log.d("error_jogador", "onFail" + t.toString());
            }
        });
    }

    private void loadDataFavoritos() {
        //Criar o fragment
        FavoritosFragment favoritosFragment = new FavoritosFragment();
        Bundle args = new Bundle();
        //args.putParcelableArrayList("jogos", (ArrayList<? extends Parcelable>) response.matches);
        favoritosFragment.setArguments(args);

        //Carregar fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.pesquisa_frag, favoritosFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void getLigas() {
        getAPI().getAllLigas(plan).enqueue(new Callback<LigaCall>() {
            @Override
            public void onResponse(Call<LigaCall> call, Response<LigaCall> response) {
                ligas = response.body().competitions;
            }

            @Override
            public void onFailure(Call<LigaCall> call, Throwable t) {

            }
        });
    }
}
