package com.example.ze_pe.dicacmu;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Logoactivity extends AppCompatActivity implements Runnable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logoactivity);

        Handler handler = new Handler();
        handler.postDelayed(this, 6000);
    }

    public void run(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}