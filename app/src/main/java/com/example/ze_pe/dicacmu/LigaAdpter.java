package com.example.ze_pe.dicacmu;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.example.ze_pe.dicacmu.API.Liga;

import java.util.ArrayList;
import java.util.List;

public class LigaAdpter extends ArrayAdapter<Liga> {
    private List<Liga> ligaListFull;

    @NonNull
    @Override
    public Filter getFilter() {
        return LigaFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.liga_autocomplete_row, parent, false);
        }
        TextView textView = convertView.findViewById(R.id.nomeLiga);
        final Liga liga = getItem(position);
        if(liga != null) {
            textView.setText(liga.getName());
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), Menu.class);
                    intent.putExtra("liga", liga.getId());
                    getContext().startActivity(intent);
                }
            });
        }
        return convertView;
    }

    private Filter LigaFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<Liga> sugestion = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                sugestion.addAll(ligaListFull);
            } else {
                String fiterPatthen = constraint.toString().toLowerCase().trim();
                for (Liga liga : ligaListFull) {
                    if (liga.getName().toLowerCase().contains(fiterPatthen)) {
                        sugestion.add(liga);
                    }
                }
            }
            results.values = sugestion;
            results.count = sugestion.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Liga) resultValue).getName();
        }
    };

    public LigaAdpter(@NonNull Context context,  @NonNull List<Liga> ligaList) {
        super(context, 0, ligaList);
        ligaListFull = new ArrayList<>(ligaList);
    }

}
