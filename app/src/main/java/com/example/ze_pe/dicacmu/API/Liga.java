package com.example.ze_pe.dicacmu.API;

import android.os.Parcel;
import android.os.Parcelable;

public class Liga implements Parcelable {
    private int id;
    private String name;

    protected Liga(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Liga> CREATOR = new Creator<Liga>() {
        @Override
        public Liga createFromParcel(Parcel in) {
            return new Liga(in);
        }

        @Override
        public Liga[] newArray(int size) {
            return new Liga[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Liga{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
