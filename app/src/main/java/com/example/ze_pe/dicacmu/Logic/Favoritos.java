package com.example.ze_pe.dicacmu.Logic;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Favoritos {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "identifica")
    private int identifica;

    @ColumnInfo(name = "nome")
    private String nome;

    @ColumnInfo(name = "tipo")
    private String tipo;

    public Favoritos(int identifica, String nome, String tipo) {
        this.identifica = identifica;
        this.nome = nome;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdentifica() {
        return identifica;
    }

    public void setIdentifica(int identifica) {
        this.identifica = identifica;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Favoritos{" +
                "id=" + id +
                ", identifica=" + identifica +
                ", nome='" + nome + '\'' +
                ", tipo='" + tipo + '\'' +
                '}';
    }
}
