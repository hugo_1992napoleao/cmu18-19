package com.example.ze_pe.dicacmu.API;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ze_pe.dicacmu.R;

import java.util.List;

public class JogadorAdapter extends RecyclerView.Adapter<JogadorAdapter.JogadorViewHolder> {

    private Context mContext;
    private List<Jogador> mJogadores;

    public JogadorAdapter(Context mContext, List<Jogador> mJogadores) {
        this.mContext = mContext;
        this.mJogadores = mJogadores;

    }

    public void setmJogos(List<Jogador> mJogadores) {
        this.mJogadores = mJogadores;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public JogadorViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View JogadorView = inflater.inflate(R.layout.item_jogador, viewGroup, false);

        return new JogadorViewHolder(JogadorView);
    }

    @Override
    public void onBindViewHolder(@NonNull JogadorViewHolder ViewHolder, int i) {
        final Jogador jogador = mJogadores.get(i);

        TextView name = ViewHolder.name;
        TextView firstName = ViewHolder.fristName;
        TextView posicao = ViewHolder.posicao;

        name.setText(jogador.getName());
        firstName.setText(jogador.getFirstName());
        posicao.setText(jogador.getPosition());

    }

    @Override
    public int getItemCount() {
        return mJogadores.size();
    }

    public class JogadorViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView fristName;
        public TextView posicao;

        public JogadorViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.jogador_name);
            fristName = itemView.findViewById(R.id.jogador_frist_name);
            posicao = itemView.findViewById(R.id.jogador_posicao);
        }
    }
}
