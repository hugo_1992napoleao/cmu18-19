package com.example.ze_pe.dicacmu.API;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ze_pe.dicacmu.Detalhes;
import com.example.ze_pe.dicacmu.Informacao;
import com.example.ze_pe.dicacmu.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class JogosAdapter extends RecyclerView.Adapter<JogosAdapter.JogosViewHolder> {

    private Context mContext;
    private List<Jogos> mJogos;

    public JogosAdapter(Context mContext, List<Jogos> mJogos) {
        this.mContext = mContext;
        this.mJogos = mJogos;

    }

    public void setmJogos(List<Jogos> mJogos) {
        this.mJogos = mJogos;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public JogosViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View JogosView = inflater.inflate(R.layout.item_jogos, viewGroup, false);

        return new JogosViewHolder(JogosView);
    }

    @Override
    public void onBindViewHolder(@NonNull JogosViewHolder jogosViewHolder, int i) {
        final Jogos jogos = mJogos.get(i);

        TextView casa = jogosViewHolder.equpa_casa;
        final TextView data = jogosViewHolder.data_jogo;
        TextView visitante = jogosViewHolder.equpa_visitante;

        casa.setText(jogos.getHomeTeam().getName());
        data.setText(jogos.getUtcDate().toString());
        visitante.setText(jogos.getAwayTeam().getName());

        casa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, Detalhes.class);
                intent.putExtra("detalhes", jogos);
                mContext.startActivity(intent);
            }
        });

        data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, Informacao.class);
                intent.putExtra("morada", jogos.getHomeTeam().getId());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mJogos.size();
    }

    public class JogosViewHolder extends RecyclerView.ViewHolder {

        public TextView equpa_visitante;
        public TextView data_jogo;
        public TextView equpa_casa;

        public JogosViewHolder(@NonNull View itemView) {
            super(itemView);
            equpa_visitante = itemView.findViewById(R.id.equpa_visitante);
            data_jogo = itemView.findViewById(R.id.data_jogo);
            equpa_casa = itemView.findViewById(R.id.equpa_casa);
        }
    }

}
