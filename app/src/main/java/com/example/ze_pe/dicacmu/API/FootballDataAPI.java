package com.example.ze_pe.dicacmu.API;

import java.util.Date;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FootballDataAPI {
    @Headers("X-Auth-Token: 1fc0a179737147c1aabeab4a92855f9c")
    @GET("/v2/competitions/{id}/matches")
    Call<LOJ> getAllGamesOfcompetition(@Path("id") int id);

    @Headers("X-Auth-Token: 1fc0a179737147c1aabeab4a92855f9c")
    @GET("/v2/competitions/{id}/matches")
    Call<LOJ> getAllGamesformStatus(@Path("id") int id,
                                    @Query("status") String status);

    @Headers("X-Auth-Token: 1fc0a179737147c1aabeab4a92855f9c")
    @GET("/v2/competitions/{id}/teams")
    Call<Eq> getAllEquipasOfcompetition(@Path("id") int id);

    @Headers("X-Auth-Token: 1fc0a179737147c1aabeab4a92855f9c")
    @GET("/v2/players/{id}/matches")
    Call<JogadorCall> getJogadorOfcompetition(@Path("id") int id,
                                              @Query("permission") String permission);

    @Headers("X-Auth-Token: 1fc0a179737147c1aabeab4a92855f9c")
    @GET("/v2/competitions/")
    Call<LigaCall> getAllLigas(@Query("plan") String plan);

    @Headers("X-Auth-Token: 1fc0a179737147c1aabeab4a92855f9c")
    @GET("/v2/matches/{id}")
    Call<Eq> getEquipaID(@Path("id") int id);

    @Headers("X-Auth-Token: 1fc0a179737147c1aabeab4a92855f9c")
    @GET("/v2/competitions/{id}/matches")
    Call<LOJ> getAllGamesformdate(@Path("id") int id,
                                  @Query("dateFrom") Date inicio,
                                  @Query("dateTo")Date fim);
}
