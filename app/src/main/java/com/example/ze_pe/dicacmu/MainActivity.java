package com.example.ze_pe.dicacmu;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, Menu.class);
        PendingIntent pendingIntent = PendingIntent
                .getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        Notification notificacao = builder
                .setSmallIcon(R.drawable.notificar)
                .setContentTitle("DicaTotal")
                .setContentText("Bem vindo a uma experiencia unica no mundo do futebol")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificacao);


        VideoView video = findViewById(R.id.videoView);
        Button botaoiniciar = findViewById(R.id.botaoiniciar);
        Button botaosair = findViewById(R.id.botaosair);






        VideoView videoView = (VideoView) findViewById(R.id.videoView);
        String uriPatch = "android.resource://com.example.ze_pe.dicacmu/" + R.raw.to;
        Uri uri2 = Uri.parse(uriPatch);
        videoView.setVideoURI(uri2);
        videoView.requestFocus();
        videoView.start();







        botaoiniciar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent it = new Intent(MainActivity.this, Menu.class);
                startActivity(it);
            }
        });

        botaosair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);

            }
        });

    }




}








