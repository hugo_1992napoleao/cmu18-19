package com.example.ze_pe.dicacmu.Logic;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface FavoritosDAO {

    @Query("SELECT * FROM favoritos")
    List<Favoritos> getAllFavoritos();

    @Insert
    void insertFavoritos(Favoritos... favoritos);

    @Delete
    void deleteFavoritos(Favoritos... favoritos);
}
