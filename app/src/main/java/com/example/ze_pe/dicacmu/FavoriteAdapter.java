package com.example.ze_pe.dicacmu;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ze_pe.dicacmu.Logic.Favoritos;
import com.example.ze_pe.dicacmu.Logic.FavoritosDataBase;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {

    private Context mContext;
    private List<Favoritos> myFavoritos;

    public FavoriteAdapter(Context mContext, List<Favoritos> myFavoritos) {
        this.mContext = mContext;
        this.myFavoritos = myFavoritos;
    }

    @Override
    public int getItemCount() {
        return myFavoritos.size();
    }

    @NonNull
    @Override
    public FavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View favoriteView = inflater.inflate(R.layout.favorite_item, parent, false);

        return new FavoriteViewHolder(favoriteView);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteViewHolder favoriteViewHolder, final int position) {
        final Favoritos favoritos = myFavoritos.get(position);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(favoritos.getId());

        TextView name = favoriteViewHolder.idView;
        TextView tipo = favoriteViewHolder.nameView;
        Button delete = favoriteViewHolder.deleteBTN;

        tipo.setText(favoritos.getTipo());
        name.setText(favoritos.getNome());

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apagarBadeDados(favoritos);
                myFavoritos.remove(position);
                notifyItemRemoved(position);
            }
        });
    }

    public void apagarBadeDados(Favoritos favoritos) {
        final FavoritosDataBase dataBase = Room.databaseBuilder(mContext, FavoritosDataBase.class, "prodution")
                .allowMainThreadQueries()
                .build();
        dataBase.favoritosDAO().deleteFavoritos(favoritos);
    }

    public class FavoriteViewHolder extends RecyclerView.ViewHolder {
        public TextView idView;
        public TextView nameView;
        public Button deleteBTN;

        public FavoriteViewHolder(@NonNull View itemView) {
            super(itemView);

            idView = itemView.findViewById(R.id.idView);
            nameView = itemView.findViewById(R.id.nameView);
            deleteBTN = itemView.findViewById(R.id.deleteBTN);
        }
    }

}
